import request from '@/utils/request'

export function searchUser(name) {
  return request({
    url: '/vue-element-admin/search/user',
    method: 'get',
    params: { name }
  })
}

export function transactionList(query) {
  return request({
    url: '/getStackingListPage',
    method: 'get',
    params: query
  })
}
export function deleteStackingTask(id) {
  return request({
    url: '/deleteStackingTask',
    method: 'get',
    params: id
  })
}

export function reSendCurrentStacking(query) {
  return request({
    url: '/reSendCurrentStacking',
    method: 'get',
    params: query
  })
}

export function reSendNextStacking(query) {
  return request({
    url: '/sendNextStacking',
    method: 'get',
    params: query
  })
}

export function reconnectPlcFromPage() {
  return request({
    url: '/reconnectPlcFromPage',
    method: 'get'
  })
}

export function closePlcConnectionFromPage() {
  return request({
    url: '/closePlcConnectionFromPage',
    method: 'get'
  })
}

export function startScadaFromServer() {
  return request({
    url: '/startScadaFromServer',
    method: 'get'
  })
}

export function closeScadaFromServer() {
  return request({
    url: '/closeScadaFromServer',
    method: 'get'
  })
}






