import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/getDcsCacheForPage',
    method: 'get',
    params: query
  })
}


export function updateArticle(data) {
  return request({
    url: '/updateDcsCacheForPage',
    method: 'post',
    data
  })
}

export function returnMaterialApi(query){
	  return request({
    url: '/goBackMaterial',
    method: 'get',
    params: query
  })
}
export function callMaterialApi(query){
	  return request({
    url: '/callMaterial',
    method: 'get',
    params: query
  })
}