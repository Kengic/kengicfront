import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/productMaterialList',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createArticle(data) {
  return request({
    url: '/productMaterialCreate',
    method: 'post',
    data
  })
}

export function updateArticle(data) {
  return request({
    url: '/productMaterialUpdate',
    method: 'post',
    data
  })
}
export function deleteArticle(id) {
  return request({
    url: '/productMaterialDelete',
    method: 'get',
    params: id
  })
}
export function getAllProduct() {
  return request({
    url: '/getProductAll',
    method: 'get'
  })
}
export function getAllMaterial() {
  return request({
    url: '/getAllMaterial',
    method: 'get'
  })
}
